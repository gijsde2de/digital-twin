﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class LaserPointer : MonoBehaviour
{

    public SteamVR_Input_Sources handType;
    public SteamVR_Behaviour_Pose controllerPose;
    public SteamVR_Action_Boolean teleportAction;
    public SteamVR_Action_Boolean backAction;

    public GameObject panoSphere;

    public GameObject laserPrefab;
    private GameObject laser;
    private Transform laserTransform;
    private Vector3 hitPoint;

    public Transform cameraRigTransform;
    public Transform headTransform;

    private Vector3 oldCameraRigPosition;
    private Vector3 oldHeadPosition;
    private bool panoMode;
    public Material panoMaterial;

    public LayerMask teleportMask;
    private bool canTeleport;


    // Start is called before the first frame update
    void Start()
    {
        if (!SteamVR.active) return;
        
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;

        canTeleport = false;
        panoMode = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!SteamVR.active) return;

        RaycastHit hit;
        if (Physics.Raycast(controllerPose.transform.position, transform.forward, out hit, 100))
        {
            hitPoint = hit.point;
            ShowLaser(hit);
            canTeleport = true;
        }
        else
        {
            laser.SetActive(false);
            canTeleport = false;
        }

        if (teleportAction.GetStateUp(handType) && canTeleport)
        {
            if (hit.transform.gameObject.name == "PanoSphere")
            {
                Pano pano = hit.transform.gameObject.GetComponent<Pano>();
                panoMaterial.mainTexture = Resources.Load("PanoMaterials/" + pano.photo) as Texture2D;

                TeleportPano();
            }
            else
            {
                Teleport();
            }
        }
        else if (backAction.GetStateUp(handType) && panoMode)
        {
            TeleportPanoBack();
        }
    }

    private void ShowLaser(RaycastHit hit)
    {
        laser.SetActive(true);
        laserTransform.position = Vector3.Lerp(controllerPose.transform.position, hitPoint, .5f);
        laserTransform.LookAt(hitPoint);
        laserTransform.localScale = new Vector3(laserTransform.localScale.x,
                                                laserTransform.localScale.y,
                                                hit.distance
                                                );
    }

    private void Teleport()
    {
        canTeleport = false;
        Vector3 difference = cameraRigTransform.position - headTransform.position;
        difference.y = 0;
        cameraRigTransform.position = hitPoint + difference;
    }

    private void TeleportPano()
    {
        panoMode = true;
        canTeleport = false;
        oldCameraRigPosition = cameraRigTransform.position;
        oldHeadPosition = headTransform.position;

        cameraRigTransform.position = Vector3.zero;
        headTransform.position = panoSphere.transform.position;
        UnityEngine.XR.InputTracking.disablePositionalTracking = true;
    }

    private void TeleportPanoBack()
    {
        cameraRigTransform.position = oldCameraRigPosition;
        headTransform.position = oldHeadPosition;
        panoMode = false;
        UnityEngine.XR.InputTracking.disablePositionalTracking = false;
    }
}
