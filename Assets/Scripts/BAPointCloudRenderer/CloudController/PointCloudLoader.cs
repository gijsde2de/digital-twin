﻿using BAPointCloudRenderer.CloudData;
using BAPointCloudRenderer.Loading;
using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace BAPointCloudRenderer.CloudController {
    /* While PointCloudLoaderController will load the complete file as one and render the comlete one, 
     * the DynamicLoaderController will first only load the hierarchy. It can be given registered at a PointCloudSetController to render it.
     */
    /// <summary>
    /// Use this script to load a single PointCloud from a directory.
    /// </summary>
    public class PointCloudLoader : MonoBehaviour {

        /// <summary>
        /// Path to the folder which contains the cloud.js file
        /// </summary>
        public string cloudPath;

        /// <summary>
        /// The PointSetController to use
        /// </summary>
        public AbstractPointCloudSet setController;

        /// <summary>
        /// True if the point cloud should be loaded when the behaviour is started. Otherwise the point cloud is loaded when LoadPointCloud is loaded.
        /// </summary>
        public bool loadOnStart = true;

        private Node rootNode;
        private List<Node> resultNodes;

        void Start() {
            if (loadOnStart) {
                LoadPointCloud();
            }
        }

        private void LoadHierarchy() {
            try {
                if (!cloudPath.EndsWith("\\")) {
                    cloudPath = cloudPath + "\\";
                }

                PointCloudMetaData metaData = CloudLoader.LoadMetaData(cloudPath, false);

                setController.UpdateBoundingBox(this, metaData.boundingBox);

                rootNode = CloudLoader.LoadHierarchyOnly(metaData);

                setController.AddRootNode(rootNode);
            } catch (Exception ex) {
                Debug.LogError(ex);
            }
        }

        /// <summary>
        /// Starts loading the point cloud. When the hierarchy is loaded it is registered at the corresponding point cloud set
        /// </summary>
        public void LoadPointCloud() {
            setController.RegisterController(this);
            Thread thread = new Thread(LoadHierarchy);
            thread.Start();
        }

        /// <summary>
        /// Removes the point cloud from the scene. Should only be called from the main thread!
        /// </summary>
        /// <returns>True if the cloud was removed. False, when the cloud hasn't even been loaded yet.</returns>
        public bool RemovePointCloud() {
            if (rootNode == null) {
                return false;
            }
            setController.RemoveRootNode(this, rootNode);
            return true;
        }

        public Vector3 IntersectRay(Ray ray)
        {
            resultNodes = new List<Node>();

            RecursiveIntersectRay(ray, rootNode);

            float distance;
            float minDistance = 20f;
            Vector3 resultPoint = new Vector3();
            Vector3[] verticeData;
            Vector3 vertice;
            foreach (Node i in resultNodes)
            {
                if (i.HasGameObjects())
                {
                    foreach (GameObject g in i.getGameObjects())
                    {
                        verticeData = g.GetComponent<MeshFilter>().mesh.vertices;
                        foreach (Vector3 v in verticeData)
                        {
                            vertice = v + g.transform.position;
                            distance = DistanceToLine(ray, vertice);
                            if (distance < minDistance)
                            {
                                // Check if point isn't behind the camera
                                if (Vector3.Dot(ray.direction, ray.origin - vertice) < 0)
                                {
                                    minDistance = distance;
                                    resultPoint = vertice;
                                }
                            }
                        }
                    }
                }
            }

            return resultPoint;
        }

        private void RecursiveIntersectRay(Ray ray, Node node)
        {
            if (node.BoundingBox.GetBoundsObject().IntersectRay(ray))
            {
                resultNodes.Add(node);
                foreach (Node i in node)
                {
                    RecursiveIntersectRay(ray, i);
                }
            }
        }

        private float DistanceToLine(Ray ray, Vector3 point)
        {
            return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
        }
    }
}
