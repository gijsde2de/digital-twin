﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BAPointCloudRenderer.CloudController;
using BAPointCloudRenderer.Controllers;

public class SceneController : MonoBehaviour
{
    private bool pressed;
    public GameObject pointCloudLoaderObject;
    private PointCloudLoader pointCloudLoader;
    public GameObject selectionPrefab;
    public GameObject myObject;

    public Camera camera;
    private Vector3 oldPosition;
    public GameObject panoSphere;

    public Material panoMaterial;
    private int panoOffset;
    private bool panoMode;

    private bool measuringMode;
    public float conversionFactor;

    // Start is called before the first frame update
    void Start()
    {
        this.pressed = true;

        panoMode = false;
        panoOffset = 0;

        measuringMode = false;

        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (pointCloudLoader == null)
        {
            pointCloudLoader = pointCloudLoaderObject.GetComponent<PointCloudLoader>();
        }

        if (Input.GetKeyDown("space"))
        {
            this.pressed = !this.pressed;
            if (this.pressed)
            {
                pointCloudLoader.LoadPointCloud();
                this.myObject.SetActive(false);
            } else
            {
                pointCloudLoader.RemovePointCloud();
                this.myObject.SetActive(true);
            }
        }

        if (Input.GetKeyDown("b"))
        {
            measuringMode = true;
        }

        if (measuringMode)
        {
            if (Input.GetMouseButtonDown(1))
            {
                measuringMode = false;
                foreach (GameObject g in GameObject.FindGameObjectsWithTag("measurePoint"))
                {
                    Destroy(g);
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = new Ray(camera.transform.position, camera.transform.forward);

                Vector3 intersection = pointCloudLoader.IntersectRay(ray);

                // Check if there already was a point selected, if so report distance
                GameObject first = GameObject.FindGameObjectWithTag("measurePoint");
                if (first)
                {
                    float measure = (first.transform.position - intersection).magnitude * conversionFactor;
                    Debug.Log("Gemeten afstand: " + measure + " cm");
                }

                float distance = Vector3.Distance(camera.transform.position, intersection);

                GameObject newObject = Instantiate(selectionPrefab, intersection, Quaternion.identity);
                newObject.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f) * distance;
                newObject.tag = "measurePoint";
            }
        }

        if (panoMode && Input.GetMouseButtonDown(1))
        {
            panoMode = false;

            camera.transform.position = oldPosition;

            CameraController cameraController = camera.GetComponent<CameraController>();
            cameraController.yaw -= panoOffset;
        }


        if (!panoMode && Input.GetMouseButtonDown(0))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.transform.gameObject.name == "PanoSphere")
                {
                    Pano pano = hit.transform.gameObject.GetComponent<Pano>();
                    panoMaterial.mainTexture = Resources.Load("PanoMaterials/" + pano.photo) as Texture2D;
                    panoOffset = pano.offset;

                    oldPosition = camera.transform.position;
                    camera.transform.position = panoSphere.transform.position;

                    CameraController cameraController = camera.GetComponent<CameraController>();
                    cameraController.yaw += panoOffset;
                    panoMode = true;
                }
            }
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

}
